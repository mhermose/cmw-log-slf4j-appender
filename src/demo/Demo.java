import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Demo{
   //private static final Logger logger = LoggerFactory.getLogger(Main.class);
   private static final Logger logger = LoggerFactory.getLogger("java.demo");

	static {
		String cwd = Paths.get(".").toAbsolutePath().normalize().toString();
		System.load(cwd + "/build/demo/Slf4jAppender.so");
	}


   public static void main(String args[]) {
      logger.info("Starting main");
      Slf4jAppenderDemo appender = new Slf4jAppenderDemo();
      appender.tick();
      logger.info("Stopping main");
   }
}
