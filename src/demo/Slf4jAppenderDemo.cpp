#include <demo/Slf4jAppenderDemo.h>
#include <iostream>
#include <cmw-log-slf4j-appender/Slf4jAppenderBuilder.h>
#include <cmw-log-slf4j-appender/Slf4jAppender.h>
#include <cmw-log/StdoutAppenderBuilder.h>
#include <cmw-log/StdoutAppender.h>
#include <cmw-log/Logger.h>
#include <cmw-log/LogManager.h>
#include <cmw-log/AppenderManager.h>

using std::cout;
using std::endl;
using cmw::log::Slf4jAppenderBuilder;
using cmw::log::StdoutAppenderBuilder;
using cmw::log::Slf4jAppender;
using cmw::log::StdoutAppender;
using cmw::log::AppenderManager::registerAppender;
using cmw::log::Logger;
using cmw::log::LoggerFactory;

Slf4jAppenderDemo::Slf4jAppenderDemo(JNIEnv * env)
{
   // SLF4J
   Slf4jAppenderBuilder builder("cpp.demo");
   builder.setJniEnv(env);

   Slf4jAppender * appender = builder.build();
   registerAppender(appender);

   Logger & logger = LoggerFactory::getLogger("Main.logger");
   LoggerFactory::getRootLogger().setLevel(cmw::log::Level::LL_TRACE, false);
   LOG_TRACE_IF(logger, "This is a cpp TRACE message");
   LOG_DEBUG_IF(logger, "This is a cpp DEBUG message");
   LOG_INFO_IF(logger, "This is a cpp INFO message");
   LOG_WARNING_IF(logger, "This is a cpp WARN message");
   LOG_ERROR_IF(logger, "This is a cpp ERROR message");
}

void Slf4jAppenderDemo::tick()
{
   Logger & logger = LoggerFactory::getLogger("Main.logger");
   LOG_INFO_IF(logger, "Tick!");
}
