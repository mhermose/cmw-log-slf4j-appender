#ifndef SLF4J_APPENDER_DEMO_H
#define SLF4J_APPENDER_DEMO_H

#include <string>
#include <jni.h>

class Slf4jAppenderDemo
{
   public:
      Slf4jAppenderDemo(JNIEnv * env);
      void tick();
};
#endif
