#ifndef CMW_LOG_SLF4J_APPENDER_H_INCLUDED
#define CMW_LOG_SLF4J_APPENDER_H_INCLUDED

#include <cmw-log/Appender.h>
#include <cmw-log/Defs.h>

#include <jni.h>
#include <iostream>
#include <string>

namespace cmw
{

namespace log
{

///
/// Redirects log output to SLF4J via JNI
///
class Slf4jAppender: public Appender
{
    /// The class is build with a builder.
    friend class Slf4jAppenderBuilder;

public:
   ~Slf4jAppender();

protected:

    /// @param name             Name of the appender
    /// @param synchronous      Synchronous flag
    /// @param filters          Filters
    /// @param JNIEnv           Pointer to the JNI environment
    Slf4jAppender(const std::string & name,
                   bool synchronous,
                   const Appender::Filters & filters,
                   JNIEnv * env);

    /// Performs the write down.
    virtual void doOutput(const LogMessage & msg);

private:
    JNIEnv * env;
    jobject logger;
    jmethodID logError;
    jmethodID logWarning;
    jmethodID logInfo;
    jmethodID logDebug;
    jmethodID logTrace;
};

} // namespace log
} // namespace cmw

#endif // CMW_LOG_SLF4J_APPENDER_H_INCLUDED
