#ifndef CMW_LOG_SLF4J_APPENDER_BUILDER_H_INCLUDED
#define CMW_LOG_SLF4J_APPENDER_BUILDER_H_INCLUDED

#include <cmw-log-slf4j-appender/Slf4jAppender.h>
#include <cmw-log/AppenderBuilder.h>
#include <cmw-log/Defs.h>
#include <jni.h>
#include <string>

namespace cmw
{

namespace log
{

///
/// Builder for SLF4J appender.
///
///
class CMW_LOG_API Slf4jAppenderBuilder : public AppenderBuilder
{
public:

    /// Ctor sets default values.
    ///
    /// @param name Name of the appender
    Slf4jAppenderBuilder(const std::string & name);

    ///
    /// Build the appender.
    ///
    virtual Slf4jAppender * build();

    Slf4jAppenderBuilder & setJniEnv(JNIEnv * env)
    {
        env_ = env;
        return *this;
    }

private:
    JNIEnv * env_;
};

} // namespace log
} // namespace cmw

#endif // CMW_LOG_SLF4J_APPENDER_BUILDER_H_INCLUDED
