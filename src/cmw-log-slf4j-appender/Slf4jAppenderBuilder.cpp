#include <cmw-log-slf4j-appender/Slf4jAppenderBuilder.h>

using namespace cmw::log;
using namespace cmw::util;

Slf4jAppenderBuilder::Slf4jAppenderBuilder(const std::string & name) :
        AppenderBuilder(name)
{
}

Slf4jAppender * Slf4jAppenderBuilder::build()
{
    return new Slf4jAppender(name_, synchronous_, filters_, env_);
}

