#include <cmw-log-slf4j-appender/Slf4jAppender.h>
#include <cmw-log/LogMessage.h>
#include <string>

using namespace cmw::log;

Slf4jAppender::Slf4jAppender(const std::string & name,
               bool synchronous,
               const Appender::Filters & filters,
               JNIEnv * env)
    : Appender(name, synchronous, filters), env(env)
{
   // @TODO: error handling
   jclass LoggerFactory = env->FindClass("org/slf4j/LoggerFactory");
   jclass Logger = env->FindClass("org/slf4j/Logger");
   this->logError = env->GetMethodID(Logger, "error", "(Ljava/lang/String;)V");
   this->logWarning = env->GetMethodID(Logger, "warn", "(Ljava/lang/String;)V");
   this->logInfo = env->GetMethodID(Logger, "info", "(Ljava/lang/String;)V");
   this->logDebug = env->GetMethodID(Logger, "debug", "(Ljava/lang/String;)V");
   this->logTrace = env->GetMethodID(Logger, "trace", "(Ljava/lang/String;)V");

   jmethodID getLogger = env->GetStaticMethodID(LoggerFactory, "getLogger", "(Ljava/lang/String;)Lorg/slf4j/Logger;");
   jobject logger = env->CallStaticObjectMethod(LoggerFactory, getLogger, env->NewStringUTF(name.c_str()));
   this->logger = env->NewGlobalRef(logger);
}

Slf4jAppender::~Slf4jAppender()
{
  env->DeleteGlobalRef(logger);
}

void Slf4jAppender::doOutput(const LogMessage & msg)
{
   switch(msg.getLevel()) {
      case cmw::log::Level::LL_AUDIT:
         // @TODO check what to do here, slf4j doesn't have an audit level it seems
         break;
      case cmw::log::Level::LL_ERROR:
         env->CallVoidMethod(logger, logError, env->NewStringUTF(msg.getMsg().c_str()));
         break;
      case cmw::log::Level::LL_WARNING:
         env->CallVoidMethod(logger, logWarning, env->NewStringUTF(msg.getMsg().c_str()));
         break;
      case cmw::log::Level::LL_INFO:
         env->CallVoidMethod(logger, logInfo, env->NewStringUTF(msg.getMsg().c_str()));
         break;
      case cmw::log::Level::LL_DEBUG:
         env->CallVoidMethod(logger, logDebug, env->NewStringUTF(msg.getMsg().c_str()));
         break;
      case cmw::log::Level::LL_TRACE:
         env->CallVoidMethod(logger, logTrace, env->NewStringUTF(msg.getMsg().c_str()));
         break;
   }
}
