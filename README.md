# Slf4jAppender

CMW appender to slf4j. This allows C++ code running inside a JVM to redirect log messages to slf4j

To compile & run the demo:
```
make run-demo
```
