# swig:
# $ swig -java -o Slf4jAppenderDemo_wrap.cpp -module Demo -c++ Slf4jAppenderDemo.h
GCC=/acc/sys/cdk/L867/g++
GCC_FLAGS=-std=c++11 -fno-strict-aliasing -fPIC -g -Wall -Wno-long-long -Wsign-compare -Wno-unused-local-typedefs -Isrc/

JNI_INCLUDES=-I./ -I${JAVA_HOME}/include -I${JAVA_HOME}/include/linux 
JAVA_JARS=".:/usr/share/java/slf4j/api.jar:/usr/share/java/slf4j/slf4j-simple.jar:/usr/share/java/slf4j/jcl-over-slf4j.jar"

CMW_VERSION=3.3.0
CMW_INCLUDES=-I/acc/local/L867/cmw/cmw-fwk/$(CMW_VERSION)/include

LIB_NAME=cmw-log-slf4j-appender
SRC=$(wildcard src/$(LIB_NAME)/*cpp)
OBJ=$(SRC:src/%cpp=build/%o)

DEMO_SRC=$(wildcard src/demo/*cpp)
DEMO_OBJ=$(DEMO_SRC:src/%cpp=build/%o)

DEMO_JAVA_SRC=$(wildcard src/demo/*java)
DEMO_JAVA_CLASS=$(DEMO_JAVA_SRC:src/%java=build/%class)

compile: $(OBJ) ;

build/%.o: src/%.cpp
	mkdir -p $(dir $@)
	$(GCC) $(GCC_FLAGS) $(CMW_INCLUDES) $(JNI_INCLUDES) -c $^ -o $@

build/%class: src/%java
	mkdir -p $(dir $@)
	javac -cp $(JAVA_JARS):$(dir $^) $^ -d $(dir $@)

build/demo/Slf4jAppender.so: $(DEMO_OBJ) $(OBJ) $(DEMO_JAVA_CLASS)
	$(GCC) -shared -o build/demo/Slf4jAppender.so \
							$(OBJ) $(DEMO_OBJ) \
							/acc/local/L867/cmw/cmw-fwk/$(CMW_VERSION)/lib/libcmw-log.a \
							/acc/local/L867/cmw/cmw-fwk/$(CMW_VERSION)/lib/libcmw-util.a \
							/acc/local/L867/cmw/cmw-fwk/$(CMW_VERSION)/lib/libboost_1_69_0_thread.a

run-demo: build/demo/Slf4jAppender.so $(DEMO_JAVA_CLASS)
	# LD_LIBRARY_PATH must contain the directory where libjvm.so is located
	export LD_LIBRARY_PATH=/acc/local/Linux/x86_64-linux-gcc/gcc82-x86_64-slc6/lib64/; \
	java -cp build/demo:$(JAVA_JARS) Demo

demo: build/demo/Slf4jAppender.so

clean:
	rm -rf build

.PHONY: clean demo run-remo compile
